package com.stc.game.kryonet.ai;

import com.stc.game.kryonet.data.entities.attacks.Attack;
import com.stc.game.kryonet.data.enums.Attacks;

import java.util.Collections;
import java.util.List;

/**
 * The type Ai.
 */
public class AI {
    /**
     * Gets best attack type.
     *
     * @param attacks the attacks
     * @return the best attack type
     */
    public static Attacks getBestAttackType(final List<Attack> attacks) {
        Collections.sort(attacks);
        Collections.reverse(attacks);
        for (Attack attack : attacks) {
            if (attack.getCooldown() <= 0f) {
                return attack.getType();
            }
        }
        return null;
    }
}
