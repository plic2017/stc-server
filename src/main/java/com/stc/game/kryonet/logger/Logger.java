package com.stc.game.kryonet.logger;

import com.esotericsoftware.minlog.Log;
import com.stc.game.kryonet.data.entities.Entity3D;

import java.util.Map;

/**
 * The type Logger.
 */
public final class Logger {
    private static final boolean DEBUG = true;

    /**
     * Log info.
     *
     * @param category the category
     * @param message  the message
     */
    public static void logInfo(final String category, final String message) {
        if (DEBUG) {
            Log.info(category, message);
        }
    }

    /**
     * Log info.
     *
     * @param category the category
     * @param map      the map
     */
    public static void logInfo(final String category, final Map<String, ? extends Entity3D> map) {
        if (DEBUG) {
            for (Map.Entry<String, ? extends Entity3D> mapEntry : map.entrySet()) {
                Log.info(category, "[ " + mapEntry.getKey() + " => " + mapEntry.getValue().toString() + " ]");
            }
        }
    }
}
