package com.stc.game.kryonet.random;

import java.util.Random;

/**
 * The type Final random.
 */
public final class FinalRandom {
    /**
     * The constant random.
     */
    public static final Random random = new Random();

    private FinalRandom() {}

    /**
     * Gets value between.
     *
     * @param minBound the min bound
     * @param maxBound the max bound
     * @return the value between
     */
    public static float getValueBetween(float minBound, float maxBound) {
        if (minBound > maxBound)
            return -1;
        if (minBound == maxBound)
            return minBound;

        return minBound + random.nextFloat() * (maxBound - minBound);
    }
}
