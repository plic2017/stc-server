package com.stc.game.kryonet.data.enums;

import lombok.Getter;

/**
 * The enum Multiplayer team.
 */
public enum MultiplayerTeam {
    /**
     * Blue team multiplayer team.
     */
    BLUE_TEAM(0),
    /**
     * Red team multiplayer team.
     */
    RED_TEAM(1);

    @Getter
    private final int multiplayerTeamId;

    MultiplayerTeam(final int multiplayerTeamId) {
        this.multiplayerTeamId = multiplayerTeamId;
    }

    /**
     * Gets id from team.
     *
     * @param team the team
     * @return the id from team
     */
    public static int getIdFromMultiplayerTeam(final MultiplayerTeam team) {
        if (team == MultiplayerTeam.BLUE_TEAM) {
            return BLUE_TEAM.getMultiplayerTeamId();
        } else if (team == MultiplayerTeam.RED_TEAM) {
            return RED_TEAM.getMultiplayerTeamId();
        }
        return -1;
    }

    /**
     * Gets multiplayer team from id.
     *
     * @param teamId the team id
     * @return the multiplayer team from id
     */
    public static MultiplayerTeam getMultiplayerTeamFromId(final int teamId) {
        if (teamId == BLUE_TEAM.getMultiplayerTeamId()) {
            return BLUE_TEAM;
        } else if (teamId == RED_TEAM.getMultiplayerTeamId()) {
            return RED_TEAM;
        }
        return null;
    }
}
