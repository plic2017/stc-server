package com.stc.game.kryonet.data.modes;

import com.stc.game.kryonet.data.enums.MultiplayerTeam;
import com.stc.game.kryonet.random.FinalRandom;
import lombok.Getter;
import lombok.Setter;

/**
 * The type Team deathmatch context.
 */
public class TeamDeathmatchContext {
    private static final int NUMBER_OF_TEAMS = MultiplayerTeam.values().length;

    @Getter @Setter
    private int blueTeamParticipants;
    @Getter @Setter
    private int redTeamParticipants;
    @Getter @Setter
    private boolean inProgress;

    /**
     * Instantiates a new Team deathmatch context.
     */
    public TeamDeathmatchContext() {
        blueTeamParticipants = 0;
        redTeamParticipants = 0;
        inProgress = true;
    }

    /**
     * Gets team.
     *
     * @return the team
     */
    public MultiplayerTeam getTeam() {
        if (blueTeamParticipants < redTeamParticipants) {
            blueTeamParticipants++;
            return MultiplayerTeam.BLUE_TEAM;
        } else if (redTeamParticipants < blueTeamParticipants) {
            redTeamParticipants++;
            return MultiplayerTeam.RED_TEAM;
        } else {
            if (FinalRandom.random.nextInt(NUMBER_OF_TEAMS) == MultiplayerTeam.BLUE_TEAM.getMultiplayerTeamId()) {
                blueTeamParticipants++;
                return MultiplayerTeam.BLUE_TEAM;
            } else {
                redTeamParticipants++;
                return MultiplayerTeam.RED_TEAM;
            }
        }
    }
}
