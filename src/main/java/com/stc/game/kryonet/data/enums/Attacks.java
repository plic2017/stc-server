package com.stc.game.kryonet.data.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * The enum Attacks.
 */
public enum Attacks {
    /**
     * Basic shot attacks.
     */
    BASIC_SHOT(0),
    /**
     * Fireball attacks.
     */
    FIREBALL(1),
    /**
     * Rocket attacks.
     */
    ROCKET(2);

    @Getter
    private static final Map<Integer, Float> attackCooldowns = new HashMap<Integer, Float>() {
        {
            put(BASIC_SHOT.getAttackId(), 0.1f);
            put(FIREBALL.getAttackId(), 3f);
            put(ROCKET.getAttackId(), 8f);
        }
    };

    @Getter
    private static final Map<Integer, Float> attackDamages = new HashMap<Integer, Float>() {
        {
            put(BASIC_SHOT.getAttackId(), 20f);
            put(FIREBALL.getAttackId(), 50f);
            put(ROCKET.getAttackId(), 150f);
        }
    };

    @Getter
    private final int attackId;

    Attacks(final int attackId) {
        this.attackId = attackId;
    }

    /**
     * Gets attack type from id.
     *
     * @param attackId the attack id
     * @return the attack type from id
     */
    public static Attacks getAttackTypeFromId(final int attackId) {
        if (attackId == BASIC_SHOT.getAttackId()) {
            return BASIC_SHOT;
        } else if (attackId == FIREBALL.getAttackId()) {
            return FIREBALL;
        } else if (attackId == ROCKET.getAttackId()) {
            return ROCKET;
        }
        return null;
    }
}
