package com.stc.game.kryonet.data.entities;

import com.stc.game.kryonet.custom.Vector3;
import lombok.Getter;
import lombok.Setter;

/**
 * The type Entity 3 d.
 */
public class Entity3D {
    /**
     * The Position.
     */
    @Getter
    protected Vector3 position;
    /**
     * The Life.
     */
    @Getter @Setter
    protected float life;

    /**
     * Instantiates a new Entity 3 d.
     */
    protected Entity3D() {}

    /**
     * Sets position.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     */
    public void setPosition(float x, float y, float z) {
        if (this.position == null) {
            this.position = new Vector3(x, y, z);
            return;
        }

        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }

    /**
     * Dispose.
     */
    protected void dispose() {
        position = null;
    }
}
