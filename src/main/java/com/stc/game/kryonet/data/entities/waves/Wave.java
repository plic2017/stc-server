package com.stc.game.kryonet.data.entities.waves;

import com.stc.game.kryonet.data.entities.enemies.Enemy;
import com.stc.game.kryonet.data.entities.enemies.EnemyBigShip;
import com.stc.game.kryonet.data.entities.enemies.EnemyMediumShip;
import com.stc.game.kryonet.data.entities.enemies.EnemySmallShip;
import com.stc.game.kryonet.data.enums.EnemyType;
import com.stc.game.kryonet.random.FinalRandom;
import lombok.Getter;

import java.util.ArrayList;

/**
 * The type Wave.
 */
public class Wave {
    @Getter
    private ArrayList<String> enemiesIds;
    @Getter
    private ArrayList<float[]> enemiesPositions;

    /**
     * Instantiates a new Wave.
     *
     * @param enemiesIds       the enemies ids
     * @param enemiesPositions the enemies positions
     */
    public Wave(ArrayList<String> enemiesIds, ArrayList<float[]> enemiesPositions) {
        this.enemiesIds = enemiesIds;
        this.enemiesPositions = enemiesPositions;
    }

    /**
     * Dispose.
     */
    public void dispose() {
        enemiesIds.clear();
        enemiesIds = null;
        enemiesPositions.clear();
        enemiesPositions = null;
    }

    /**
     * Gets random enemy.
     *
     * @return the random enemy
     */
    public static Enemy getRandomEnemy() {
        int enemyTypeId = FinalRandom.random.nextInt(EnemyType.getMAX_ID());
        if (enemyTypeId == EnemyType.ENEMY_SMALL_SHIP.getEnemyTypeId()) {
            return new EnemySmallShip();
        } else if (enemyTypeId == EnemyType.ENEMY_MEDIUM_SHIP.getEnemyTypeId()) {
            return new EnemyMediumShip();
        } else {
            return new EnemyBigShip();
        }
    }
}
