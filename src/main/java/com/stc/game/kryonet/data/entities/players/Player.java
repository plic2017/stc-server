package com.stc.game.kryonet.data.entities.players;

import com.esotericsoftware.kryonet.Connection;
import com.stc.game.kryonet.data.entities.Entity3D;
import com.stc.game.kryonet.data.enums.MultiplayerTeam;
import lombok.Getter;
import lombok.Setter;

/**
 * The type Player.
 */
public class Player extends Entity3D {
    @Getter @Setter
    private Connection connection;
    @Getter @Setter
    private MultiplayerTeam team;

    /**
     * Instantiates a new Player.
     */
    public Player() {}

    @Override
    public void dispose() {
        super.dispose();
        if (connection != null) {
            connection.close();
            connection = null;
        }
        team = null;
    }
}
