package com.stc.game.kryonet.data.enums;

import lombok.Getter;

/**
 * The enum Multiplayer type.
 */
public enum MultiplayerType {
    /**
     * Realtime multiplayer multiplayer type.
     */
    REALTIME_MULTIPLAYER(0),
    /**
     * Nearby connections multiplayer multiplayer type.
     */
    NEARBY_CONNECTIONS_MULTIPLAYER(1);

    @Getter
    private final int multiplayerTypeId;

    MultiplayerType(final int multiplayerTypeId) {
        this.multiplayerTypeId = multiplayerTypeId;
    }

    /**
     * Gets multiplayer type from id.
     *
     * @param multiplayerTypeId the multiplayer type id
     * @return the multiplayer type from id
     */
    public static MultiplayerType getMultiplayerTypeFromId(final int multiplayerTypeId) {
        if (multiplayerTypeId == REALTIME_MULTIPLAYER.getMultiplayerTypeId()) {
            return REALTIME_MULTIPLAYER;
        } else if (multiplayerTypeId == NEARBY_CONNECTIONS_MULTIPLAYER.getMultiplayerTypeId()) {
            return NEARBY_CONNECTIONS_MULTIPLAYER;
        }
        return null;
    }
}
