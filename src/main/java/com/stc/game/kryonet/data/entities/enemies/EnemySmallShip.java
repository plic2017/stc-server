package com.stc.game.kryonet.data.entities.enemies;

import com.stc.game.kryonet.data.enums.Attacks;
import com.stc.game.kryonet.data.enums.EnemyType;

/**
 * The type Enemy small ship.
 */
public class EnemySmallShip extends Enemy {
    /**
     * Instantiates a new Enemy small ship.
     */
    public EnemySmallShip() {
        super();
        this.life = 100;
        this.enemyType = EnemyType.ENEMY_SMALL_SHIP;
        initAvailableAttackTypes();
        initAvailableAttacks();
    }

    @Override
    void initAvailableAttackTypes() {
        super.initAvailableAttackTypes();
        availableAttackTypes.add(Attacks.BASIC_SHOT);
    }
}
