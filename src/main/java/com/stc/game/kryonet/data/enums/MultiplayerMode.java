package com.stc.game.kryonet.data.enums;

import lombok.Getter;

/**
 * The enum Multiplayer mode.
 */
public enum  MultiplayerMode {
    /**
     * Survival multiplayer mode.
     */
    SURVIVAL(0),
    /**
     * Deathmatch multiplayer mode.
     */
    DEATHMATCH(1),
    /**
     * Team deathmatch multiplayer mode.
     */
    TEAM_DEATHMATCH(2);

    @Getter
    private final int multiplayerModeId;

    MultiplayerMode(int multiplayerModeId) {
        this.multiplayerModeId = multiplayerModeId;
    }

    /**
     * Gets multiplayer mode from id.
     *
     * @param multiplayerModeId the multiplayer mode id
     * @return the multiplayer mode from id
     */
    public static MultiplayerMode getMultiplayerModeFromId(final int multiplayerModeId) {
        if (multiplayerModeId == SURVIVAL.getMultiplayerModeId()) {
            return SURVIVAL;
        } else if (multiplayerModeId == DEATHMATCH.getMultiplayerModeId()) {
            return DEATHMATCH;
        } else if (multiplayerModeId == TEAM_DEATHMATCH.getMultiplayerModeId()){
            return TEAM_DEATHMATCH;
        }
        return null;
    }
}
