package com.stc.game.kryonet.data.entities.enemies;

import com.stc.game.kryonet.data.enums.Attacks;
import com.stc.game.kryonet.data.enums.EnemyType;

import java.util.Collections;

/**
 * The type Enemy big ship.
 */
public class EnemyBigShip extends Enemy {
    /**
     * Instantiates a new Enemy big ship.
     */
    public EnemyBigShip() {
        super();
        this.life = 200;
        this.enemyType = EnemyType.ENEMY_BIG_SHIP;
        initAvailableAttackTypes();
        initAvailableAttacks();
    }

    @Override
    void initAvailableAttackTypes() {
        super.initAvailableAttackTypes();
        Collections.addAll(availableAttackTypes, Attacks.values());
    }
}
