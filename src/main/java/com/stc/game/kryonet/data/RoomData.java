package com.stc.game.kryonet.data;

import com.esotericsoftware.kryonet.Connection;
import com.stc.game.kryonet.data.entities.enemies.Enemy;
import com.stc.game.kryonet.data.entities.players.Player;
import com.stc.game.kryonet.data.entities.waves.Wave;
import com.stc.game.kryonet.data.enums.Attacks;
import com.stc.game.kryonet.data.enums.Bonus;
import com.stc.game.kryonet.data.enums.MultiplayerMode;
import com.stc.game.kryonet.data.enums.MultiplayerTeam;
import com.stc.game.kryonet.data.enums.MultiplayerType;
import com.stc.game.kryonet.data.modes.TeamDeathmatchContext;
import com.stc.game.kryonet.logger.Logger;
import com.stc.game.kryonet.packets.Packet;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketBonus;
import com.stc.game.kryonet.packets.PacketGameOver;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketRemoveTeamDeathmatchTicket;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.kryonet.random.FinalRandom;
import com.stc.game.kryonet.scheduler.Scheduler;
import com.stc.game.kryonet.utils.KryonetPacketBuilder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * The type Room data.
 */
public class RoomData {
    private static final int Z_DEFAULT_POSITION = 0;
    private static final String HOST_IDENTIFIER = "h_";
    private static final String ENEMY_IDENTIFIER = "e_";

    @Getter @Setter
    private String hostName;

    @Getter @Setter
    private boolean cleanableRoom;

    @Getter
    private Map<String, Enemy> enemies;
    @Getter
    private Map<String, Enemy> enemiesToClear;

    @Getter
    private Map<String, Player> players;

    @Setter
    private int waveDifficulty;
    @Getter
    private int numberOfParticipants;
    @Setter
    private int numberOfPlayersReady;

    @Getter
    private Scheduler scheduler;

    @Getter @Setter
    private MultiplayerType multiplayerType;
    @Getter @Setter
    private MultiplayerMode multiplayerMode;

    private TeamDeathmatchContext teamDeathmatchContext;

    /**
     * Instantiates a new Room data.
     */
    public RoomData() {
        hostName = null;

        cleanableRoom = false;

        enemies = new HashMap<>();
        enemiesToClear = new HashMap<>();

        players = new HashMap<>();

        scheduler = new Scheduler(this);

        multiplayerType = null;
        multiplayerMode = null;

        teamDeathmatchContext = null;
    }

    /**
     * Update enemy.
     *
     * @param packetAttackEffectEntityCollision the packet attack effect entity collision
     */
    public void updateEnemy(final PacketAttackEffectEntityCollision packetAttackEffectEntityCollision) {
        String enemyId = packetAttackEffectEntityCollision.attackedEntityId;
        if (enemies.containsKey(enemyId)) {
            float life = enemies.get(enemyId).getLife();
            float damages = Attacks.getAttackDamages().get(packetAttackEffectEntityCollision.attackId);
            enemies.get(enemyId).setLife(life - (damages * packetAttackEffectEntityCollision.damageCoef));
            if (enemies.get(enemyId).getLife() <= 0) {
                deleteEnemy(enemyId);
            }
        } else {
            Player player = players.get(packetAttackEffectEntityCollision.entityId);
            if (player != null) {
                player.getConnection().sendUDP(KryonetPacketBuilder.getPacketRemoveEnemy(enemyId));
            }
        }
    }

    /**
     * Save player connection.
     *
     * @param playerName the player name
     * @param connection the connection
     */
    public void savePlayerConnection(final String playerName, final Connection connection) {
        if (!players.containsKey(playerName)) {
            Player player = new Player();
            player.setConnection(connection);
            players.put(playerName, player);
        } else {
            players.get(playerName).setConnection(connection);
        }
    }

    /**
     * Send host name.
     *
     * @param playerName the player name
     */
    public void sendHostName(final String playerName) {
        if (hostName == null) {
            if (multiplayerType == MultiplayerType.REALTIME_MULTIPLAYER) {
                hostName = getRandomHostName();
            } else if (multiplayerType == MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER) {
                hostName = getNearbyConnectionsHostName();
            }

            if (hostName == null) {
                cleanableRoom = true;
                return;
            }

            scheduler.schedulePingCurrentHost();
            scheduler.schedulePingPlayers();
        }

        Player player = players.get(playerName);
        if (player != null) {
            player.getConnection().sendUDP(KryonetPacketBuilder.getPacketSendHost(hostName));
        }

        Logger.logInfo("SEND_HOST_NAME", "The host name (" + hostName + ") has been sent to a player");
    }

    /**
     * Send packet to others players.
     *
     * @param packet the packet
     */
    public void sendPacketToOthersPlayers(Packet packet) {
        packet = compressPacket(packet);
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            String playerName = playerEntry.getKey();
            if (!playerName.equals(packet.entityId)) {
                Player player = playerEntry.getValue();
                player.getConnection().sendUDP(packet);
            }
        }
    }

    /**
     * Send packet to have been disconnected.
     *
     * @param packet               the packet
     * @param haveBeenDisconnected the have been disconnected
     */
    public void sendPacketToHaveBeenDisconnected(Packet packet, final List<String> haveBeenDisconnected) {
        if (haveBeenDisconnected == null || haveBeenDisconnected.isEmpty()) {
            return;
        }

        packet = compressPacket(packet);
        for (String playerName : haveBeenDisconnected) {
            Player player = players.get(playerName);
            if (player != null) {
                player.getConnection().sendUDP(packet);
            }
        }
    }

    /**
     * Send packet to others players of same team.
     *
     * @param packet the packet
     */
    public void sendPacketToOthersPlayersOfSameTeam(Packet packet) {
        String playerName = packet.entityId;
        Player player = players.get(playerName);
        if (player == null || player.getTeam() == null) {
            return;
        }

        packet = compressPacket(packet);
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            String otherPlayerName = playerEntry.getKey();
            Player otherPlayer = playerEntry.getValue();
            if (!playerName.equals(otherPlayerName) && player.getTeam() == otherPlayer.getTeam()) {
                otherPlayer.getConnection().sendUDP(packet);
            }
        }
    }

    /**
     * Send packet to have been disconnected of same team.
     *
     * @param packet               the packet
     * @param haveBeenDisconnected the have been disconnected
     */
    public void sendPacketToHaveBeenDisconnectedOfSameTeam(Packet packet, final List<String> haveBeenDisconnected) {
        if (haveBeenDisconnected == null || haveBeenDisconnected.isEmpty()) {
            return;
        }

        Player player = players.get(packet.entityId);
        if (player == null || player.getTeam() == null) {
            return;
        }

        packet = compressPacket(packet);
        for (String playerName : haveBeenDisconnected) {
            Player otherPlayer = players.get(playerName);
            if (otherPlayer != null && player.getTeam() == otherPlayer.getTeam()) {
                otherPlayer.getConnection().sendUDP(packet);
            }
        }
    }

    /**
     * Wait all participants.
     *
     * @param numberOfParticipants the number of participants
     */
    public void waitAllParticipants(final int numberOfParticipants) {
        Logger.logInfo("WAIT_ALL_PARTICIPANTS", "Number of participants set to " + numberOfParticipants);
        this.numberOfParticipants = numberOfParticipants;
        scheduler.scheduleWaitAllParticipants();
    }

    /**
     * Send wave to players.
     */
    public void sendWaveToPlayers() {
        Wave wave = generateWave();

        if (numberOfParticipants == 1) {
            chunkifyAndSendWave(hostName, wave.getEnemiesIds(), wave.getEnemiesPositions());
            Logger.logInfo("SEND_WAVE_TO_PLAYERS", "A wave has been sent to the last player remaining (the host)");
        } else {
            for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
                String playerName = playerEntry.getKey();
                if (!playerName.equals(hostName)) {
                    chunkifyAndSendWave(playerName, wave.getEnemiesIds(), wave.getEnemiesPositions());
                }
            }
            Logger.logInfo("SEND_WAVE_TO_PLAYERS", "A wave has been sent to " + (numberOfParticipants - 1) + " players");
        }

        waveDifficulty++;
        wave.dispose();
    }

    /**
     * Update number of players ready.
     */
    public void updateNumberOfPlayersReady() {
        numberOfPlayersReady++;
        if (numberOfPlayersReady == numberOfParticipants - 1) {
            ArrayList<String> enemiesIds = new ArrayList<>();
            ArrayList<float[]> enemiesPositions = new ArrayList<>();

            for (Map.Entry<String, Enemy> enemyEntry : enemies.entrySet()) {
                String enemyId = enemyEntry.getKey();
                Enemy enemy = enemyEntry.getValue();
                enemiesIds.add(enemyId);
                enemiesPositions.add(new float[] { enemy.getPosition().x, enemy.getPosition().y });
            }

            Logger.logInfo("UPDATE_NUMBER_OF_PLAYERS_READY", "All players are ready, sending the wave to the host...");
            chunkifyAndSendWave(hostName, enemiesIds, enemiesPositions);
            numberOfPlayersReady = 0;
        }
    }

    /**
     * Send new host name.
     */
    public void sendNewHostName() {
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            Player player = playerEntry.getValue();
            player.getConnection().sendUDP(KryonetPacketBuilder.getPacketSendHost(hostName));
        }
        Logger.logInfo("SEND_NEW_HOST_NAME", "The new host name (" + hostName + ") has been sent to the remaining players");
    }

    /**
     * Inform players of host disconnection.
     */
    public void informPlayersOfHostDisconnection() {
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            Player player = playerEntry.getValue();
            player.getConnection().sendUDP(KryonetPacketBuilder.getPacketHostDisconnection());
        }
    }

    /**
     * Inform players of winning.
     *
     * @param packetGameOver the packet game over
     */
    public void informPlayersOfWinning(final PacketGameOver packetGameOver) {
        if (teamDeathmatchContext.isInProgress()) {
            MultiplayerTeam loserTeam = MultiplayerTeam.getMultiplayerTeamFromId(packetGameOver.loserTeamId);
            for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
                Player player = playerEntry.getValue();
                if (player.getTeam() != loserTeam) {
                    player.getConnection().sendUDP(KryonetPacketBuilder.getPacketWin());
                }
            }
            teamDeathmatchContext.setInProgress(false);
        }
    }

    /**
     * Dispose.
     */
    public void dispose() {
        Logger.logInfo("PLAYERS_CONTENT", players);
        disposePlayers();
        players = null;

        Logger.logInfo("ENEMIES_CONTENT", enemies);
        disposeEnemies();
        enemies = null;

        Logger.logInfo("ENEMIES_TO_CLEAR_CONTENT", enemiesToClear);
        disposeEnemiesToClear();
        enemiesToClear = null;

        scheduler.disposeAllScheduledTasks();
        scheduler.setRoomData(null);
        scheduler = null;

        multiplayerType = null;
        multiplayerMode = null;

        teamDeathmatchContext = null;
    }

    /**
     * Gets random host name.
     *
     * @return the random host name
     */
    public String getRandomHostName() {
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            Player player = playerEntry.getValue();
            if (player.getConnection().isConnected()) {
                return playerEntry.getKey();
            }
        }
        return null;
    }

    /**
     * Delete player.
     *
     * @param playerName the player name
     */
    public void deletePlayer(final String playerName) {
        updateTeamDeathmatchContext(players.get(playerName));
        players.get(playerName).dispose();
        players.remove(playerName);
        numberOfParticipants = players.size();
        if (numberOfParticipants == 0) {
            cleanableRoom = true;
        } else {
            sendPacketToAllPlayers(KryonetPacketBuilder.getPacketRemovePlayer(playerName));
        }
        Logger.logInfo("DELETE_PLAYER", "The player " + playerName + " has been deleted");
    }

    /**
     * Send chat message.
     *
     * @param packetPlayerMessage the packet player message
     */
    public void sendChatMessage(final PacketPlayerMessage packetPlayerMessage) {
        String recipientId = packetPlayerMessage.recipient;
        if (recipientId == null) {
            if (packetPlayerMessage.hasBeenDisconnected) {
                if (multiplayerMode == MultiplayerMode.TEAM_DEATHMATCH) {
                    sendPacketToOthersPlayersOfSameTeam(packetPlayerMessage);
                } else {
                    sendPacketToOthersPlayers(packetPlayerMessage);
                }
            } else {
                if (multiplayerMode == MultiplayerMode.TEAM_DEATHMATCH) {
                    sendPacketToHaveBeenDisconnectedOfSameTeam(packetPlayerMessage, packetPlayerMessage.haveBeenDisconnected);
                } else {
                    sendPacketToHaveBeenDisconnected(packetPlayerMessage, packetPlayerMessage.haveBeenDisconnected);
                }
            }
        } else {
            Player recipient = players.get(recipientId);
            if (recipient != null) {
                recipient.getConnection().sendUDP(packetPlayerMessage);
            }
        }
    }

    /**
     * Affect player to a team.
     *
     * @param playerName the player name
     */
    public void affectPlayerToATeam(final String playerName) {
        if (teamDeathmatchContext == null) {
            teamDeathmatchContext = new TeamDeathmatchContext();
        }

        if (players.containsKey(playerName)) {
            players.get(playerName).setTeam(teamDeathmatchContext.getTeam());

            Player player = players.get(playerName);
            PacketTeamAffectation packetTeamAffectation = KryonetPacketBuilder.getPacketTeamAffectation(playerName, player.getTeam());
            player.getConnection().sendUDP(packetTeamAffectation);

            for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
                String otherPlayerName = playerEntry.getKey();
                if (!playerName.equals(otherPlayerName)) {
                    Player otherPlayer = playerEntry.getValue();
                    otherPlayer.getConnection().sendUDP(packetTeamAffectation);
                    PacketTeamAffectation otherPacketTeamAffectation = KryonetPacketBuilder.getPacketTeamAffectation(otherPlayerName, otherPlayer.getTeam());
                    player.getConnection().sendUDP(otherPacketTeamAffectation);
                }
            }
        }
    }

    /**
     * Answer to team affectation request.
     *
     * @param playerName the player name
     * @param connection the connection
     */
    public void answerToTeamAffectationRequest(final String playerName, final Connection connection) {
        Player player = players.get(playerName);
        if (player != null && player.getTeam() != null) {
            connection.sendUDP(KryonetPacketBuilder.getPacketTeamAffectation(playerName, player.getTeam()));
        }
    }

    private void deleteEnemy(final String enemyId) {
        if (!enemiesToClear.containsKey(enemyId) && enemies.containsKey(enemyId)) {
            int faceId = enemies.get(enemyId).getEnemyFace().getFaceId();
            enemiesToClear.put(enemyId, enemies.get(enemyId));
            sendPacketToAllPlayers(KryonetPacketBuilder.getPacketRemoveEnemy(enemyId));
            float random = FinalRandom.getValueBetween(0, 1);
            if (random <= 0.80f) {
                int bonusId;
                if (random <= 0.30f) {
                    bonusId = Bonus.LIFE_BONUS.getBonusId();
                } else if (random <= 0.50f) {
                    bonusId = Bonus.COOLDOWN_BONUS.getBonusId();
                } else if (random <= 60f) {
                    bonusId = Bonus.POWER_BONUS.getBonusId();
                } else {
                    bonusId = Bonus.INVINCIBLE_BONUS.getBonusId();
                }
                PacketBonus packetBonus = KryonetPacketBuilder.getPacketBonus(bonusId, faceId);
                for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
                    Player player = playerEntry.getValue();
                    player.getConnection().sendUDP(packetBonus);
                }
            }
        }
    }

    private Wave generateWave() {
        if (!enemies.isEmpty()) {
            disposeEnemies();
        }

        ArrayList<String> enemiesIds = new ArrayList<>();
        ArrayList<float[]> enemiesPositions = new ArrayList<>();

        for (int i = 0; i < waveDifficulty; i++) {
            Enemy enemy = Wave.getRandomEnemy();

            String enemyId = ENEMY_IDENTIFIER +
                             enemy.getEnemyType().getEnemyTypeId() + "_" +
                             enemy.getEnemyColor().getColorId() + "_" +
                             enemy.getEnemyFace().getFaceId() + "_" +
                             UUID.randomUUID().toString().replace("-", "");
            float x = FinalRandom.random.nextFloat() * 20;
            float y = FinalRandom.random.nextFloat() * 20;
            enemy.setPosition(x, y, Z_DEFAULT_POSITION);

            enemies.put(enemyId, enemy);
            enemiesIds.add(enemyId);
            enemiesPositions.add(new float[] { x, y });
        }

        return new Wave(enemiesIds, enemiesPositions);
    }

    private String getNearbyConnectionsHostName() {
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            String playerName = playerEntry.getKey();
            if (playerName.startsWith(HOST_IDENTIFIER)) {
                return playerName;
            }
        }
        return null;
    }

    private void chunkifyAndSendWave(final String playerName, final ArrayList<String> enemiesIds, final ArrayList<float[]> enemiesPositions) {
        int numberOfEnemies = enemiesIds.size();
        if (numberOfEnemies == 1) {
            Player player = players.get(playerName);
            if (player != null) {
                player.getConnection().sendUDP(KryonetPacketBuilder.getPacketSendWave(enemiesIds, enemiesPositions, true));
            }
            return;
        }

        int chunkSize = 2;
        boolean oddNumberOfEnemies = numberOfEnemies % chunkSize == 1;
        int numberOfChunks = (numberOfEnemies / chunkSize) + (oddNumberOfEnemies ? 1 : 0);

        for (int i = 0; i < numberOfChunks; i++) {
            boolean onLastChunk = i == numberOfChunks - 1;
            ArrayList<String> enemiesIdsChunk = new ArrayList<>();
            ArrayList<float[]> enemiesPositionsChunk = new ArrayList<>();
            for (int j = 0; j < chunkSize; j++) {
                enemiesIdsChunk.add(enemiesIds.get((i * chunkSize) + j));
                enemiesPositionsChunk.add(enemiesPositions.get((i * chunkSize) + j));
                if (onLastChunk && oddNumberOfEnemies) {
                    break;
                }
            }
            Player player = players.get(playerName);
            if (player != null) {
                player.getConnection().sendUDP(KryonetPacketBuilder.getPacketSendWave(enemiesIdsChunk, enemiesPositionsChunk, onLastChunk));
            }
        }
    }

    private void updateTeamDeathmatchContext(final Player player) {
        if (teamDeathmatchContext == null) {
            return;
        }

        if (player.getTeam() == MultiplayerTeam.BLUE_TEAM) {
            teamDeathmatchContext.setBlueTeamParticipants(teamDeathmatchContext.getBlueTeamParticipants() - 1);
        } else {
            teamDeathmatchContext.setRedTeamParticipants(teamDeathmatchContext.getRedTeamParticipants() - 1);
        }
    }

    /**
     * Send packet to all players.
     *
     * @param packet the packet
     */
    public void sendPacketToAllPlayers(final Packet packet) {
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            Player player = playerEntry.getValue();
            player.getConnection().sendUDP(packet);
        }
    }

    private void disposePlayers() {
        for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
            playerEntry.getValue().dispose();
        }
        players.clear();
    }

    private void disposeEnemies() {
        for (Map.Entry<String, Enemy> enemyEntry : enemies.entrySet()) {
            enemyEntry.getValue().dispose();
        }
        enemies.clear();
    }

    private void disposeEnemiesToClear() {
        for (Map.Entry<String, Enemy> enemyEntry : enemiesToClear.entrySet()) {
            enemyEntry.getValue().dispose();
        }
        enemiesToClear.clear();
    }

    private static Packet compressPacket(final Packet packet) {
        if (packet instanceof PacketPlayer) {
            ((PacketPlayer) packet).haveBeenDisconnected = null;
        } else if (packet instanceof PacketPlayerAttack) {
            ((PacketPlayerAttack) packet).haveBeenDisconnected = null;
        } else if (packet instanceof PacketAttackEffectEntityCollision) {
            ((PacketAttackEffectEntityCollision) packet).haveBeenDisconnected = null;
        } else if (packet instanceof PacketPlayerMessage) {
            ((PacketPlayerMessage) packet).haveBeenDisconnected = null;
        } else if (packet instanceof PacketRemoveTeamDeathmatchTicket) {
            ((PacketRemoveTeamDeathmatchTicket) packet).haveBeenDisconnected = null;
        }
        return packet;
    }
}
