package com.stc.game.kryonet.data.enums;

import lombok.Getter;

/**
 * The enum Bonus.
 */
public enum Bonus {
    /**
     * Cooldown bonus bonus.
     */
    COOLDOWN_BONUS(0),
    /**
     * Invincible bonus bonus.
     */
    INVINCIBLE_BONUS(1),
    /**
     * Life bonus bonus.
     */
    LIFE_BONUS(2),
    /**
     * Power bonus bonus.
     */
    POWER_BONUS(3);

    @Getter
    private final int bonusId;

    Bonus(final int bonusId) {
        this.bonusId = bonusId;
    }
}
