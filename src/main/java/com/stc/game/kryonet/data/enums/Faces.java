package com.stc.game.kryonet.data.enums;

import com.stc.game.kryonet.random.FinalRandom;
import lombok.Getter;

/**
 * The enum Faces.
 */
public enum Faces {
    /**
     * Front faces.
     */
    FRONT(0), // +Z
    /**
     * Back faces.
     */
    BACK(1), // -Z
    /**
     * Left faces.
     */
    LEFT(2), // -X
    /**
     * Right faces.
     */
    RIGHT(3); // +X

    private static final int MAX_ID = Faces.values().length;

    @Getter
    private final int faceId;

    Faces(final int faceId) {
        this.faceId = faceId;
    }

    /**
     * Gets random face.
     *
     * @return the random face
     */
    public static Faces getRandomFace() {
        return Faces.values()[FinalRandom.random.nextInt(MAX_ID)];
    }
}
