package com.stc.game.kryonet.data.enums;

import com.stc.game.kryonet.random.FinalRandom;
import lombok.Getter;

/**
 * The enum Color.
 */
public enum Color {
    /**
     * Blue color.
     */
    BLUE(0),
    /**
     * Green color.
     */
    GREEN(1),
    /**
     * Red color.
     */
    RED(2);

    private static final int MAX_ID = Color.values().length;

    @Getter
    private final int colorId;

    Color(final int colorId) {
        this.colorId = colorId;
    }

    /**
     * Gets random color.
     *
     * @return the random color
     */
    public static Color getRandomColor() {
        return Color.values()[FinalRandom.random.nextInt(MAX_ID)];
    }
}
