package com.stc.game.kryonet.data.entities.enemies;

import com.stc.game.kryonet.data.enums.Attacks;
import com.stc.game.kryonet.data.enums.EnemyType;

/**
 * The type Enemy medium ship.
 */
public class EnemyMediumShip extends Enemy {
    /**
     * Instantiates a new Enemy medium ship.
     */
    public EnemyMediumShip() {
        super();
        this.life = 150;
        this.enemyType = EnemyType.ENEMY_MEDIUM_SHIP;
        initAvailableAttackTypes();
        initAvailableAttacks();
    }

    @Override
    void initAvailableAttackTypes() {
        super.initAvailableAttackTypes();
        availableAttackTypes.add(Attacks.BASIC_SHOT);
        availableAttackTypes.add(Attacks.FIREBALL);
    }
}
