package com.stc.game.kryonet.data.entities.enemies;

import com.stc.game.kryonet.data.entities.attacks.Attack;
import com.stc.game.kryonet.data.entities.Entity3D;
import com.stc.game.kryonet.data.enums.Attacks;
import com.stc.game.kryonet.data.enums.Color;
import com.stc.game.kryonet.data.enums.EnemyType;
import com.stc.game.kryonet.data.enums.Faces;
import com.stc.game.kryonet.random.FinalRandom;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Enemy.
 */
public class Enemy extends Entity3D {
    /**
     * The Available attack types.
     */
    List<Attacks> availableAttackTypes;

    /**
     * The Available attacks.
     */
    @Getter
    protected List<Attack> availableAttacks;
    /**
     * The Enemy type.
     */
    @Getter
    protected EnemyType enemyType;
    /**
     * The Enemy color.
     */
    @Getter
    protected Color enemyColor;
    /**
     * The Enemy face.
     */
    @Getter
    protected Faces enemyFace;
    @Getter @Setter
    private String targetId;

    /**
     * Instantiates a new Enemy.
     */
    Enemy() {
        this.enemyColor = Color.getRandomColor();
        this.enemyFace = Faces.getRandomFace();
    }

    /**
     * Reset attack cooldown.
     *
     * @param attackType the attack type
     */
    public void resetAttackCooldown(final Attacks attackType) {
        for (Attack attack : availableAttacks) {
            if (attack.getType() == attackType) {
                int attackId = attackType.getAttackId();
                float cooldown = Attacks.getAttackCooldowns().get(attackId) +
                        (attackType == Attacks.BASIC_SHOT ? getDeltaCooldown() : 0);
                attack.setCooldown(cooldown);
                return;
            }
        }
    }

    /**
     * Sets attack cooldown.
     *
     * @param attackType the attack type
     * @param cooldown   the cooldown
     */
    public void setAttackCooldown(final Attacks attackType, final float cooldown) {
        for (Attack attack : availableAttacks) {
            if (attack.getType() == attackType) {
                attack.setCooldown(cooldown);
                return;
            }
        }
    }

    /**
     * Init available attack types.
     */
    void initAvailableAttackTypes() {
        availableAttackTypes = new ArrayList<>();
    }

    /**
     * Init available attacks.
     */
    void initAvailableAttacks() {
        availableAttacks = new ArrayList<>();
        for (Attacks attackType : availableAttackTypes) {
            int attackId = attackType.getAttackId();
            float cooldown = Attacks.getAttackCooldowns().get(attackId) +
                    (attackType == Attacks.BASIC_SHOT ? getDeltaCooldown() : 0);
            float damage = Attacks.getAttackDamages().get(attackId);
            Attack attack = new Attack();
            attack.setType(attackType);
            attack.setCooldown(cooldown);
            attack.setDamage(damage);
            availableAttacks.add(attack);
        }
    }

    private static float getDeltaCooldown() {
        return FinalRandom.getValueBetween(0.5f, 1.0f);
    }

    @Override
    public void dispose() {
        super.dispose();
        if (availableAttackTypes != null) {
            availableAttackTypes.clear();
            availableAttackTypes = null;
        }
        if (availableAttacks != null) {
            availableAttacks.clear();
            availableAttacks = null;
        }
        enemyType = null;
        enemyColor = null;
        enemyFace = null;
        targetId = null;
    }
}
