package com.stc.game.kryonet.data.entities.attacks;

import com.stc.game.kryonet.data.enums.Attacks;
import lombok.Getter;
import lombok.Setter;

/**
 * The type Attack.
 */
public class Attack implements Comparable<Attack> {
    @Getter @Setter
    private Attacks type;
    @Getter @Setter
    private float cooldown;
    @Getter @Setter
    private float damage;

    /**
     * Instantiates a new Attack.
     */
    public Attack() {}

    @Override
    public int compareTo(final Attack attack) {
        if (damage - attack.getDamage() > 0)
            return 1;
        else if (damage - attack.getDamage() < 0)
            return -1;
        return 0;
    }
}
