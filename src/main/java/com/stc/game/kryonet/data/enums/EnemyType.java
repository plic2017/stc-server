package com.stc.game.kryonet.data.enums;

import lombok.Getter;

/**
 * The enum Enemy type.
 */
public enum EnemyType {
    /**
     * Enemy small ship enemy type.
     */
    ENEMY_SMALL_SHIP(0),
    /**
     * Enemy medium ship enemy type.
     */
    ENEMY_MEDIUM_SHIP(1),
    /**
     * Enemy big ship enemy type.
     */
    ENEMY_BIG_SHIP(2);

    @Getter
    private static final int MAX_ID = EnemyType.values().length;

    @Getter
    private final int enemyTypeId;

    EnemyType(final int enemyTypeId) {
        this.enemyTypeId = enemyTypeId;
    }
}
