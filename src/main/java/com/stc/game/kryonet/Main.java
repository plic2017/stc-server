package com.stc.game.kryonet;

import com.stc.game.kryonet.server.KryonetServer;

import java.io.IOException;

/**
 * The type Main.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        try {
            new KryonetServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
