package com.stc.game.kryonet.custom;

/**
 * The type Vector 3.
 */
public final class Vector3 {
    /**
     * The X.
     */
    public float x;
    /**
     * The Y.
     */
    public float y;
    /**
     * The Z.
     */
    public float z;

    /**
     * Instantiates a new Vector 3.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     */
    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
