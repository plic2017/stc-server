package com.stc.game.kryonet.packets;

/**
 * The type Packet enemy attack.
 */
public class PacketEnemyAttack extends Packet {
    /**
     * The Attack id.
     */
    public int attackId;
}
