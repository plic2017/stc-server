package com.stc.game.kryonet.packets;

import java.util.ArrayList;

/**
 * The type Packet player.
 */
public class PacketPlayer extends Packet {
    /**
     * The X position.
     */
    public float xPosition;
    /**
     * The Y position.
     */
    public float yPosition;
    /**
     * The Z position.
     */
    public float zPosition;
    /**
     * The X direction.
     */
    public float xDirection;
    /**
     * The Y direction.
     */
    public float yDirection;
    /**
     * The Z direction.
     */
    public float zDirection;
    /**
     * The W direction.
     */
    public float wDirection;
    /**
     * The Has been disconnected.
     */
    public boolean hasBeenDisconnected;
    /**
     * The Have been disconnected.
     */
    public ArrayList<String> haveBeenDisconnected;
}
