package com.stc.game.kryonet.packets;

import java.util.ArrayList;

/**
 * The type Packet player attack.
 */
public class PacketPlayerAttack extends Packet {
    /**
     * The Attack id.
     */
    public int attackId;
    /**
     * The Color id.
     */
    public int colorId;
    /**
     * The Target id.
     */
    public String targetId;
    /**
     * The Has been disconnected.
     */
    public boolean hasBeenDisconnected;
    /**
     * The Have been disconnected.
     */
    public ArrayList<String> haveBeenDisconnected;
}
