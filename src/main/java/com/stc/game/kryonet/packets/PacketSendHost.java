package com.stc.game.kryonet.packets;

/**
 * The type Packet send host.
 */
public class PacketSendHost {
    /**
     * The Host name.
     */
    public String hostName;
}
