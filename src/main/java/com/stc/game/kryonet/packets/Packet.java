package com.stc.game.kryonet.packets;

/**
 * The type Packet.
 */
public class Packet {
    /**
     * The Entity id.
     */
    public String entityId;
    /**
     * The Room id.
     */
    public String roomId;
}
