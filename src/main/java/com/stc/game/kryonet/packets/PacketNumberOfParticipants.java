package com.stc.game.kryonet.packets;

/**
 * The type Packet number of participants.
 */
public class PacketNumberOfParticipants extends Packet {
    /**
     * The Number of participants.
     */
    public int numberOfParticipants;
}
