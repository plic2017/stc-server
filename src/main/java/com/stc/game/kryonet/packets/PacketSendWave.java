package com.stc.game.kryonet.packets;

import java.util.ArrayList;

/**
 * The type Packet send wave.
 */
public class PacketSendWave {
    /**
     * The Enemies ids.
     */
    public ArrayList<String> enemiesIds;
    /**
     * The Enemies positions.
     */
    public ArrayList<float[]> enemiesPositions;
    /**
     * The Complete.
     */
    public boolean complete;
}
