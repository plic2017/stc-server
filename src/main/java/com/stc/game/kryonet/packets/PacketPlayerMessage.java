package com.stc.game.kryonet.packets;

import java.util.ArrayList;

/**
 * The type Packet player message.
 */
public class PacketPlayerMessage extends Packet {
    /**
     * The Recipient.
     */
    public String recipient;
    /**
     * The Message.
     */
    public String message;
    /**
     * The Has been disconnected.
     */
    public boolean hasBeenDisconnected;
    /**
     * The Have been disconnected.
     */
    public ArrayList<String> haveBeenDisconnected;
}
