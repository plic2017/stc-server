package com.stc.game.kryonet.packets;

import java.util.ArrayList;

/**
 * The type Packet attack effect entity collision.
 */
public class PacketAttackEffectEntityCollision extends Packet {
    /**
     * The Attack id.
     */
    public int attackId;
    /**
     * The Damage coef.
     */
    public float damageCoef;
    /**
     * The Attacked entity id.
     */
    public String attackedEntityId;
    /**
     * The Has been disconnected.
     */
    public boolean hasBeenDisconnected;
    /**
     * The Have been disconnected.
     */
    public ArrayList<String> haveBeenDisconnected;
}
