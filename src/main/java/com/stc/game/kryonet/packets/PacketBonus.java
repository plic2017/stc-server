package com.stc.game.kryonet.packets;

/**
 * The type Packet bonus.
 */
public class PacketBonus {
    /**
     * The Bonus id.
     */
    public int bonusId;
    /**
     * The Face id.
     */
    public int faceId;
}
