package com.stc.game.kryonet.packets;

/**
 * The type Packet game over.
 */
public class PacketGameOver extends Packet {
    /**
     * The Loser team id.
     */
    public int loserTeamId;
}
