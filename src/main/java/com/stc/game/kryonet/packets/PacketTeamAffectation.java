package com.stc.game.kryonet.packets;

/**
 * The type Packet team affectation.
 */
public class PacketTeamAffectation extends Packet {
    /**
     * The Team id.
     */
    public int teamId;
}
