package com.stc.game.kryonet.packets;

import java.util.ArrayList;

/**
 * The type Packet remove team deathmatch ticket.
 */
public class PacketRemoveTeamDeathmatchTicket extends Packet {
    /**
     * The Has been disconnected.
     */
    public boolean hasBeenDisconnected;
    /**
     * The Have been disconnected.
     */
    public ArrayList<String> haveBeenDisconnected;
}
