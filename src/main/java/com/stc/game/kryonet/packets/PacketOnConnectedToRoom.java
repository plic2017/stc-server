package com.stc.game.kryonet.packets;

/**
 * The type Packet on connected to room.
 */
public class PacketOnConnectedToRoom extends Packet {
    /**
     * The Multiplayer type id.
     */
    public int multiplayerTypeId;
    /**
     * The Multiplayer mode id.
     */
    public int multiplayerModeId;
}
