package com.stc.game.kryonet.server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Server;
import com.stc.game.kryonet.listener.NetworkListener;
import com.stc.game.kryonet.packets.Packet;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketBonus;
import com.stc.game.kryonet.packets.PacketEnemyAttack;
import com.stc.game.kryonet.packets.PacketGameOver;
import com.stc.game.kryonet.packets.PacketGetHost;
import com.stc.game.kryonet.packets.PacketGetWave;
import com.stc.game.kryonet.packets.PacketHostDisconnection;
import com.stc.game.kryonet.packets.PacketNumberOfParticipants;
import com.stc.game.kryonet.packets.PacketOnConnectedToRoom;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketPlayerReady;
import com.stc.game.kryonet.packets.PacketReconnection;
import com.stc.game.kryonet.packets.PacketRemoveEnemy;
import com.stc.game.kryonet.packets.PacketRemovePlayer;
import com.stc.game.kryonet.packets.PacketRemoveTeamDeathmatchTicket;
import com.stc.game.kryonet.packets.PacketSendHost;
import com.stc.game.kryonet.packets.PacketSendWave;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.kryonet.packets.PacketWin;

import java.io.IOException;
import java.util.ArrayList;

/**
 * The type Kryonet server.
 */
public final class KryonetServer {
    private static final int TCP_PORT = 54555;
    private static final int UDP_PORT = 54777;

    private final Server server;

    /**
     * Instantiates a new Kryonet server.
     *
     * @throws IOException the io exception
     */
    public KryonetServer() throws IOException {
        server = new Server();
        registerPackets();
        server.addListener(new NetworkListener());
        server.bind(TCP_PORT, UDP_PORT);
        server.start();
    }

    private void registerPackets() {
        Kryo kryo = server.getKryo();

        kryo.register(float[].class);
        kryo.register(ArrayList.class);

        kryo.register(Packet.class);
        kryo.register(PacketPlayer.class);
        kryo.register(PacketEnemyAttack.class);
        kryo.register(PacketGetHost.class);
        kryo.register(PacketSendHost.class);
        kryo.register(PacketOnConnectedToRoom.class);
        kryo.register(PacketRemovePlayer.class);
        kryo.register(PacketPlayerAttack.class);
        kryo.register(PacketRemoveEnemy.class);
        kryo.register(PacketAttackEffectEntityCollision.class);
        kryo.register(PacketGetWave.class);
        kryo.register(PacketSendWave.class);
        kryo.register(PacketNumberOfParticipants.class);
        kryo.register(PacketPlayerReady.class);
        kryo.register(PacketPlayerMessage.class);
        kryo.register(PacketReconnection.class);
        kryo.register(PacketTeamAffectation.class);
        kryo.register(PacketRemoveTeamDeathmatchTicket.class);
        kryo.register(PacketHostDisconnection.class);
        kryo.register(PacketBonus.class);
        kryo.register(PacketGameOver.class);
        kryo.register(PacketWin.class);
    }
}
