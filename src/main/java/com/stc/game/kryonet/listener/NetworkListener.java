package com.stc.game.kryonet.listener;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.stc.game.kryonet.data.RoomData;
import com.stc.game.kryonet.data.enums.MultiplayerMode;
import com.stc.game.kryonet.data.enums.MultiplayerType;
import com.stc.game.kryonet.logger.Logger;
import com.stc.game.kryonet.packets.Packet;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketGameOver;
import com.stc.game.kryonet.packets.PacketGetHost;
import com.stc.game.kryonet.packets.PacketGetWave;
import com.stc.game.kryonet.packets.PacketNumberOfParticipants;
import com.stc.game.kryonet.packets.PacketOnConnectedToRoom;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketPlayerReady;
import com.stc.game.kryonet.packets.PacketReconnection;
import com.stc.game.kryonet.packets.PacketRemoveTeamDeathmatchTicket;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.kryonet.security.Security;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The type Network listener.
 */
public class NetworkListener extends Listener {
    private static final long NOW = 0L;
    private static final long THIRTY_SECONDS = 1000L * 30L;

    private static Map<String, RoomData> rooms;

    /**
     * Instantiates a new Network listener.
     */
    public NetworkListener() {
        super();
        rooms = new HashMap<>();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                cleanRooms();
            }
        }, NOW, THIRTY_SECONDS);
    }

    @Override
    public void received(Connection connection, Object object) {
        if (object == null) {
            return;
        }

        if (object instanceof Packet && Security.isValidPacket((Packet) object)) {
            if (object instanceof PacketOnConnectedToRoom && Security.isValidPacketOnConnectedToRoom((PacketOnConnectedToRoom) object)) {
                handlePacketOnConnectedToRoom((PacketOnConnectedToRoom) object, connection);
                return;
            }
            if (object instanceof PacketGetHost) {
                handlePacketGetHost((PacketGetHost) object);
                return;
            }
            if (object instanceof PacketPlayer) {
                handlePacketPlayer((PacketPlayer) object);
                return;
            }
            if (object instanceof PacketPlayerAttack && Security.isValidPacketPlayerAttack((PacketPlayerAttack) object)) {
                handlePacketPlayerAttack((PacketPlayerAttack) object);
                return;
            }
            if (object instanceof PacketAttackEffectEntityCollision && Security.isValidPacketAttackEffectEntityCollision((PacketAttackEffectEntityCollision) object)) {
                handlePacketAttackEffectEntityCollision((PacketAttackEffectEntityCollision) object);
                return;
            }
            if (object instanceof PacketNumberOfParticipants && Security.isValidPacketNumberOfParticipants((PacketNumberOfParticipants) object)) {
                handlePacketNumberOfParticipants((PacketNumberOfParticipants) object);
                return;
            }
            if (object instanceof PacketPlayerReady) {
                handlePacketPlayerReady((PacketPlayerReady) object);
                return;
            }
            if (object instanceof PacketGetWave) {
                handlePacketGetWave((PacketGetWave) object);
                return;
            }
            if (object instanceof PacketPlayerMessage && Security.isValidPacketPlayerMessage((PacketPlayerMessage) object)) {
                handlePacketPlayerMessage((PacketPlayerMessage) object);
                return;
            }
            if (object instanceof PacketReconnection) {
                handlePacketReconnection((PacketReconnection) object);
                return;
            }
            if (object instanceof PacketTeamAffectation) {
                handlePacketTeamAffection((PacketTeamAffectation) object, connection);
                return;
            }
            if (object instanceof PacketRemoveTeamDeathmatchTicket) {
                handlePacketRemoveTeamDeathmatchTicket((PacketRemoveTeamDeathmatchTicket) object);
                return;
            }
            if (object instanceof PacketGameOver && Security.isValidPacketGameOver((PacketGameOver) object)) {
                handlePacketGameOver((PacketGameOver) object);
            }
        }
    }

    private static void handlePacketOnConnectedToRoom(final PacketOnConnectedToRoom packetOnConnectedToRoom, final Connection connection) {
        String roomId = packetOnConnectedToRoom.roomId;
        String playerName = packetOnConnectedToRoom.entityId;

        Logger.logInfo("HANDLE_PACKET_ON_CONNECTED_TO_ROOM", "Player " + playerName + " has connected to room " + roomId);

        if (!rooms.containsKey(roomId)) {
            rooms.put(roomId, new RoomData());
        }

        rooms.get(roomId).savePlayerConnection(playerName, connection);

        if (rooms.get(roomId).getMultiplayerMode() == null && rooms.get(roomId).getMultiplayerType() == null) {
            MultiplayerType multiplayerType = MultiplayerType.getMultiplayerTypeFromId(packetOnConnectedToRoom.multiplayerTypeId);
            MultiplayerMode multiplayerMode = MultiplayerMode.getMultiplayerModeFromId(packetOnConnectedToRoom.multiplayerModeId);
            rooms.get(roomId).setMultiplayerType(multiplayerType);
            rooms.get(roomId).setMultiplayerMode(multiplayerMode);
            if (multiplayerMode == MultiplayerMode.SURVIVAL) {
                rooms.get(roomId).setWaveDifficulty(1);
                rooms.get(roomId).setNumberOfPlayersReady(0);
                rooms.get(roomId).getScheduler().scheduleCleanEnemies();
                rooms.get(roomId).getScheduler().scheduleUpdateAttackCooldowns();
            } else if (multiplayerMode == MultiplayerMode.DEATHMATCH ||
                       multiplayerMode == MultiplayerMode.TEAM_DEATHMATCH) {
                rooms.get(roomId).getScheduler().schedulePingPlayers();
            }
        }

        if (rooms.get(roomId).getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
            rooms.get(roomId).affectPlayerToATeam(playerName);
        }
    }

    private static void handlePacketPlayer(final PacketPlayer packetPlayer) {
        String roomId = packetPlayer.roomId;
        if (rooms.containsKey(roomId)) {
            if (rooms.get(roomId).getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
                if (packetPlayer.hasBeenDisconnected) {
                    rooms.get(roomId).sendPacketToOthersPlayers(packetPlayer);
                } else {
                    rooms.get(roomId).sendPacketToHaveBeenDisconnected(packetPlayer, packetPlayer.haveBeenDisconnected);
                }
            }
        }
    }

    private static void handlePacketGetHost(final PacketGetHost packetGetHost) {
        String roomId = packetGetHost.roomId;
        if (rooms.containsKey(roomId)) {
            rooms.get(roomId).sendHostName(packetGetHost.entityId);
        }
    }

    private static void handlePacketNumberOfParticipants(final PacketNumberOfParticipants packetNumberOfParticipants) {
        String roomId = packetNumberOfParticipants.roomId;
        if (rooms.containsKey(roomId)) {
            rooms.get(roomId).waitAllParticipants(packetNumberOfParticipants.numberOfParticipants);
        }
    }

    private static void handlePacketPlayerReady(final PacketPlayerReady packetPlayerReady) {
        String roomId = packetPlayerReady.roomId;
        if (rooms.containsKey(roomId)) {
            rooms.get(roomId).updateNumberOfPlayersReady();
        }
    }

    private static void handlePacketPlayerAttack(final PacketPlayerAttack packetPlayerAttack) {
        String roomId = packetPlayerAttack.roomId;
        if (rooms.containsKey(roomId)) {
            if (packetPlayerAttack.hasBeenDisconnected) {
                rooms.get(roomId).sendPacketToOthersPlayers(packetPlayerAttack);
            } else {
                rooms.get(roomId).sendPacketToHaveBeenDisconnected(packetPlayerAttack, packetPlayerAttack.haveBeenDisconnected);
            }
        }
    }

    private static void handlePacketAttackEffectEntityCollision(final PacketAttackEffectEntityCollision packetAttackEffectEntityCollision) {
        String roomId = packetAttackEffectEntityCollision.roomId;
        if (rooms.containsKey(roomId)) {
            if (rooms.get(roomId).getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
                rooms.get(roomId).updateEnemy(packetAttackEffectEntityCollision);
            }
            if (packetAttackEffectEntityCollision.hasBeenDisconnected) {
                rooms.get(roomId).sendPacketToOthersPlayers(packetAttackEffectEntityCollision);
            } else {
                rooms.get(roomId).sendPacketToHaveBeenDisconnected(packetAttackEffectEntityCollision, packetAttackEffectEntityCollision.haveBeenDisconnected);
            }
        }
    }

    private static void handlePacketGetWave(final PacketGetWave packetGetWave) {
        String roomId = packetGetWave.roomId;
        if (rooms.containsKey(roomId)) {
            Logger.logInfo("HANDLE_PACKET_GET_WAVE", "A wave has been asked by the host (" + packetGetWave.entityId + ")");
            rooms.get(roomId).sendWaveToPlayers();
        }
    }

    private static void handlePacketPlayerMessage(final PacketPlayerMessage packetPlayerMessage) {
        String roomId = packetPlayerMessage.roomId;
        if (rooms.containsKey(roomId)) {
            rooms.get(roomId).sendChatMessage(packetPlayerMessage);
        }
    }

    private static void handlePacketReconnection(final PacketReconnection packetReconnection) {
        String roomId = packetReconnection.roomId;
        if (rooms.containsKey(roomId)) {
            rooms.get(roomId).sendPacketToOthersPlayers(packetReconnection);
        }
    }

    private static void handlePacketTeamAffection(final PacketTeamAffectation packetTeamAffectation, final Connection connection) {
        String roomId = packetTeamAffectation.roomId;
        if (rooms.containsKey(roomId)) {
            rooms.get(roomId).answerToTeamAffectationRequest(packetTeamAffectation.entityId, connection);
        }
    }

    private static void handlePacketRemoveTeamDeathmatchTicket(final PacketRemoveTeamDeathmatchTicket packetRemoveTeamDeathmatchTicket) {
        String roomId = packetRemoveTeamDeathmatchTicket.roomId;
        if (rooms.containsKey(roomId)) {
            if (packetRemoveTeamDeathmatchTicket.hasBeenDisconnected) {
                rooms.get(roomId).sendPacketToOthersPlayersOfSameTeam(packetRemoveTeamDeathmatchTicket);
            } else {
                rooms.get(roomId).sendPacketToHaveBeenDisconnectedOfSameTeam(packetRemoveTeamDeathmatchTicket, packetRemoveTeamDeathmatchTicket.haveBeenDisconnected);
            }
        }
    }

    private static void handlePacketGameOver(final PacketGameOver packetGameOver) {
        String roomId = packetGameOver.roomId;
        if (rooms.containsKey(roomId)) {
            rooms.get(roomId).informPlayersOfWinning(packetGameOver);
        }
    }

    private static void cleanRooms() {
        Iterator<Map.Entry<String, RoomData>> it = rooms.entrySet().iterator();

        if (!it.hasNext()) {
            return;
        }

        while (it.hasNext()) {
            Map.Entry<String, RoomData> roomEntry = it.next();
            String roomId = roomEntry.getKey();
            RoomData roomData = roomEntry.getValue();
            if (roomData.isCleanableRoom()) {
                roomData.dispose();
                it.remove();
                Logger.logInfo("CLEAN_ROOMS", "Cleaning of the room " + roomId + "... Number of remaining rooms : " + rooms.size());
            }
        }
    }
}
