package com.stc.game.kryonet.utils;

import com.stc.game.kryonet.data.enums.MultiplayerTeam;
import com.stc.game.kryonet.packets.PacketBonus;
import com.stc.game.kryonet.packets.PacketEnemyAttack;
import com.stc.game.kryonet.packets.PacketHostDisconnection;
import com.stc.game.kryonet.packets.PacketRemoveEnemy;
import com.stc.game.kryonet.packets.PacketRemovePlayer;
import com.stc.game.kryonet.packets.PacketSendHost;
import com.stc.game.kryonet.packets.PacketSendWave;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.kryonet.packets.PacketWin;

import java.util.ArrayList;

/**
 * The type Kryonet packet builder.
 */
public final class KryonetPacketBuilder {
    /**
     * Gets packet enemy attack.
     *
     * @param enemyId  the enemy id
     * @param attackId the attack id
     * @return the packet enemy attack
     */
    public static PacketEnemyAttack getPacketEnemyAttack(final String enemyId, final int attackId) {
        PacketEnemyAttack packetEnemyAttack = new PacketEnemyAttack();
        packetEnemyAttack.entityId = enemyId;
        packetEnemyAttack.attackId = attackId;
        return packetEnemyAttack;
    }

    /**
     * Gets packet send host.
     *
     * @param hostName the host name
     * @return the packet send host
     */
    public static PacketSendHost getPacketSendHost(final String hostName) {
        PacketSendHost packetSendHost = new PacketSendHost();
        packetSendHost.hostName = hostName;
        return packetSendHost;
    }

    /**
     * Gets packet remove player.
     *
     * @param playerName the player name
     * @return the packet remove player
     */
    public static PacketRemovePlayer getPacketRemovePlayer(final String playerName) {
        PacketRemovePlayer packetRemovePlayer = new PacketRemovePlayer();
        packetRemovePlayer.entityId = playerName;
        return packetRemovePlayer;
    }

    /**
     * Gets packet remove enemy.
     *
     * @param enemyId the enemy id
     * @return the packet remove enemy
     */
    public static PacketRemoveEnemy getPacketRemoveEnemy(final String enemyId) {
        PacketRemoveEnemy packetRemoveEnemy = new PacketRemoveEnemy();
        packetRemoveEnemy.entityId = enemyId;
        return packetRemoveEnemy;
    }

    /**
     * Gets packet send wave.
     *
     * @param enemiesIds       the enemies ids
     * @param enemiesPositions the enemies positions
     * @param complete         the complete
     * @return the packet send wave
     */
    public static PacketSendWave getPacketSendWave(final ArrayList<String> enemiesIds, final ArrayList<float[]> enemiesPositions, final boolean complete) {
        PacketSendWave packetSendWave = new PacketSendWave();
        packetSendWave.enemiesIds = enemiesIds;
        packetSendWave.enemiesPositions = enemiesPositions;
        packetSendWave.complete = complete;
        return packetSendWave;
    }

    /**
     * Gets packet team affectation.
     *
     * @param playerName the player name
     * @param team       the team
     * @return the packet team affectation
     */
    public static PacketTeamAffectation getPacketTeamAffectation(final String playerName, final MultiplayerTeam team) {
        PacketTeamAffectation packetTeamAffectation = new PacketTeamAffectation();
        packetTeamAffectation.entityId = playerName;
        packetTeamAffectation.teamId = MultiplayerTeam.getIdFromMultiplayerTeam(team);
        return packetTeamAffectation;
    }

    /**
     * Gets packet host disconnection.
     *
     * @return the packet host disconnection
     */
    public static PacketHostDisconnection getPacketHostDisconnection() {
        return new PacketHostDisconnection();
    }

    /**
     * Gets packet bonus.
     *
     * @param bonusId the bonus id
     * @param faceId  the face id
     * @return the packet bonus
     */
    public static PacketBonus getPacketBonus(final int bonusId, final int faceId) {
        PacketBonus packetBonus = new PacketBonus();
        packetBonus.bonusId = bonusId;
        packetBonus.faceId = faceId;
        return packetBonus;
    }

    /**
     * Gets packet win.
     *
     * @return the packet win
     */
    public static PacketWin getPacketWin() {
        return new PacketWin();
    }
}
