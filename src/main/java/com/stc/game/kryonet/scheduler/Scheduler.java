package com.stc.game.kryonet.scheduler;

import com.stc.game.kryonet.ai.AI;
import com.stc.game.kryonet.data.RoomData;
import com.stc.game.kryonet.data.entities.attacks.Attack;
import com.stc.game.kryonet.data.entities.enemies.Enemy;
import com.stc.game.kryonet.data.entities.players.Player;
import com.stc.game.kryonet.data.enums.Attacks;
import com.stc.game.kryonet.data.enums.MultiplayerMode;
import com.stc.game.kryonet.data.enums.MultiplayerType;
import com.stc.game.kryonet.logger.Logger;
import com.stc.game.kryonet.utils.KryonetPacketBuilder;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The type Scheduler.
 */
public class Scheduler {
    private static final long NOW = 0L;
    private static final long ONE_MILLISECOND = 1L;
    private static final long ONE_SECOND = 1000L;
    private static final long FIVE_SECONDS = 1000L * 5L;

    @Setter
    private RoomData roomData;

    private Timer timerForPingingCurrentHost;
    private Timer timerForPingingPlayers;
    private Timer timerForWaitingAllParticipants;
    private Timer timerForCleaningEnemies;
    private Timer timerForUpdatingAttackCooldowns;
    private Timer timerForSendingAttackInstructions;

    /**
     * Instantiates a new Scheduler.
     *
     * @param roomData the room data
     */
    public Scheduler(RoomData roomData) {
        this.roomData = roomData;

        timerForPingingCurrentHost = new Timer();
        timerForPingingPlayers = new Timer();
        timerForWaitingAllParticipants = new Timer();
        timerForCleaningEnemies = new Timer();
        timerForUpdatingAttackCooldowns = new Timer();
        timerForSendingAttackInstructions = new Timer();
    }

    /**
     * Schedule ping current host.
     */
    public void schedulePingCurrentHost() {
        timerForPingingCurrentHost.schedule(new TimerTask() {
            @Override
            public void run() {
                if (roomData.isCleanableRoom()) {
                    cancel();
                    return;
                }

                String hostName = roomData.getHostName();
                Player host = roomData.getPlayers().get(hostName);
                if (host != null && !host.getConnection().isConnected()) {
                    roomData.deletePlayer(hostName);
                    if (mustFindAnotherHostName()) {
                        hostName = roomData.getRandomHostName();

                        if (hostName == null) {
                            roomData.setCleanableRoom(true);
                            return;
                        }

                        roomData.setHostName(hostName);
                        roomData.sendNewHostName();
                    } else {
                        roomData.informPlayersOfHostDisconnection();
                    }
                }
            }
        }, NOW, FIVE_SECONDS);
    }

    /**
     * Schedule ping players.
     */
    public void schedulePingPlayers() {
        timerForPingingPlayers.schedule(new TimerTask() {
            @Override
            public void run() {
                if (roomData.isCleanableRoom() || (roomData.getMultiplayerMode() == MultiplayerMode.SURVIVAL && roomData.getNumberOfParticipants() == 1)) {
                    cancel();
                    return;
                }

                Map<String, Player> players = new HashMap<>(roomData.getPlayers());
                for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
                    String playerName = playerEntry.getKey();
                    Player player = playerEntry.getValue();
                    if (roomData.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
                        String hostName = roomData.getHostName();
                        if (!playerName.equals(hostName) && !player.getConnection().isConnected()) {
                            roomData.deletePlayer(playerName);
                        }
                    } else if (roomData.getMultiplayerMode() == MultiplayerMode.DEATHMATCH ||
                               roomData.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
                        if (!player.getConnection().isConnected()) {
                            roomData.deletePlayer(playerName);
                        }
                    }
                }
            }
        }, NOW, FIVE_SECONDS);
    }

    /**
     * Schedule wait all participants.
     */
    public void scheduleWaitAllParticipants() {
        Logger.logInfo("SCHEDULE_WAIT_ALL_PARTICIPANTS", "Waiting for all participants to connect");
        timerForWaitingAllParticipants.schedule(new TimerTask() {
            @Override
            public void run() {
                if (roomData.isCleanableRoom()) {
                    cancel();
                    return;
                }

                int expectedNumberOfParticipants = roomData.getNumberOfParticipants();
                if (roomData.getPlayers().size() == expectedNumberOfParticipants) {
                    Logger.logInfo("SCHEDULE_WAIT_ALL_PARTICIPANTS", "All participants are connected");
                    roomData.sendWaveToPlayers();
                    scheduleSendAttackInstructions();
                    cancel();
                }
            }
        }, NOW, ONE_SECOND);
    }

    /**
     * Schedule clean enemies.
     */
    public void scheduleCleanEnemies() {
        timerForCleaningEnemies.schedule(new TimerTask() {
            @Override
            public void run() {
                if (roomData.isCleanableRoom()) {
                    cancel();
                    return;
                }

                Logger.logInfo("ENEMIES_TO_CLEAR_CONTENT", roomData.getEnemiesToClear());

                Map<String, Enemy> enemiesToClear = new HashMap<>(roomData.getEnemiesToClear());
                for (Map.Entry<String, Enemy> enemyEntry : enemiesToClear.entrySet()) {
                    String enemyId = enemyEntry.getKey();
                    if (roomData.getEnemies().containsKey(enemyId)) {
                        roomData.getEnemies().get(enemyId).dispose();
                        roomData.getEnemies().remove(enemyId);
                        Logger.logInfo("SCHEDULE_CLEAN_ENEMIES", "The enemy " + enemyId + " has been deleted");
                    }
                }

                roomData.getEnemiesToClear().clear();
            }
        }, NOW, FIVE_SECONDS);
    }

    /**
     * Dispose all scheduled tasks.
     */
    public void disposeAllScheduledTasks() {
        timerForPingingCurrentHost.cancel();
        timerForPingingCurrentHost.purge();
        timerForPingingCurrentHost = null;

        timerForPingingPlayers.cancel();
        timerForPingingPlayers.purge();
        timerForPingingPlayers = null;

        timerForWaitingAllParticipants.cancel();
        timerForWaitingAllParticipants.purge();
        timerForWaitingAllParticipants = null;

        timerForCleaningEnemies.cancel();
        timerForCleaningEnemies.purge();
        timerForCleaningEnemies = null;

        timerForUpdatingAttackCooldowns.cancel();
        timerForUpdatingAttackCooldowns.purge();
        timerForUpdatingAttackCooldowns = null;

        timerForSendingAttackInstructions.cancel();
        timerForSendingAttackInstructions.purge();
        timerForSendingAttackInstructions = null;
    }

    /**
     * Schedule update attack cooldowns.
     */
    public void scheduleUpdateAttackCooldowns() {
        timerForUpdatingAttackCooldowns.schedule(new TimerTask() {
            @Override
            public void run() {
                if (roomData.isCleanableRoom()) {
                    cancel();
                    return;
                }

                Map<String, Enemy> enemies = new HashMap<>(roomData.getEnemies());
                for (Map.Entry<String, Enemy> enemyEntry : enemies.entrySet()) {
                    Enemy enemy = enemyEntry.getValue();
                    for (Attack attack : enemy.getAvailableAttacks()) {
                        if (attack.getCooldown() > 0f) {
                            String enemyId = enemyEntry.getKey();
                            if (roomData.getEnemies().containsKey(enemyId)) {
                                float cooldown = attack.getCooldown() - 0.001f;
                                roomData.getEnemies().get(enemyId).setAttackCooldown(attack.getType(), cooldown <= 0f ? 0f : cooldown);
                            }
                        }
                    }
                }
            }
        }, NOW, ONE_MILLISECOND);
    }

    private void scheduleSendAttackInstructions() {
        timerForSendingAttackInstructions.schedule(new TimerTask() {
            @Override
            public void run() {
                if (roomData.isCleanableRoom()) {
                    cancel();
                    return;
                }

                Map<String, Enemy> enemies = new HashMap<>(roomData.getEnemies());
                for (Map.Entry<String, Enemy> enemyEntry : enemies.entrySet()) {
                    String enemyId = enemyEntry.getKey();
                    Enemy enemy = enemyEntry.getValue();
                    Attacks bestAttackType = AI.getBestAttackType(enemy.getAvailableAttacks());
                    if (bestAttackType != null && roomData.getEnemies().containsKey(enemyId)) {
                        roomData.getEnemies().get(enemyId).resetAttackCooldown(bestAttackType);
                        roomData.sendPacketToAllPlayers(KryonetPacketBuilder.getPacketEnemyAttack(enemyId, bestAttackType.getAttackId()));
                    }
                }
            }
        }, NOW, ONE_MILLISECOND);
    }

    private boolean mustFindAnotherHostName() {
        return roomData.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER &&
               roomData.getMultiplayerMode() == MultiplayerMode.SURVIVAL;
    }
}
