package com.stc.game.kryonet.security;

import com.stc.game.kryonet.data.enums.Attacks;
import com.stc.game.kryonet.data.enums.MultiplayerMode;
import com.stc.game.kryonet.data.enums.MultiplayerTeam;
import com.stc.game.kryonet.data.enums.MultiplayerType;
import com.stc.game.kryonet.packets.Packet;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketGameOver;
import com.stc.game.kryonet.packets.PacketNumberOfParticipants;
import com.stc.game.kryonet.packets.PacketOnConnectedToRoom;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;

/**
 * The type Security.
 */
public final class Security {
    private static final int MAX_MESSAGE_LENGTH = 128;

    /**
     * Is valid packet boolean.
     *
     * @param packet the packet
     * @return the boolean
     */
    public static boolean isValidPacket(final Packet packet) {
        return packet.entityId != null && packet.roomId != null;
    }

    /**
     * Is valid packet on connected to room boolean.
     *
     * @param packetOnConnectedToRoom the packet on connected to room
     * @return the boolean
     */
    public static boolean isValidPacketOnConnectedToRoom(final PacketOnConnectedToRoom packetOnConnectedToRoom) {
        return MultiplayerType.getMultiplayerTypeFromId(packetOnConnectedToRoom.multiplayerTypeId) != null &&
               MultiplayerMode.getMultiplayerModeFromId(packetOnConnectedToRoom.multiplayerModeId) != null;
    }

    /**
     * Is valid packet player attack boolean.
     *
     * @param packetPlayerAttack the packet player attack
     * @return the boolean
     */
    public static boolean isValidPacketPlayerAttack(final PacketPlayerAttack packetPlayerAttack) {
        return Attacks.getAttackTypeFromId(packetPlayerAttack.attackId) != null;
    }

    /**
     * Is valid packet attack effect entity collision boolean.
     *
     * @param packetAttackEffectEntityCollision the packet attack effect entity collision
     * @return the boolean
     */
    public static boolean isValidPacketAttackEffectEntityCollision(final PacketAttackEffectEntityCollision packetAttackEffectEntityCollision) {
        return Attacks.getAttackTypeFromId(packetAttackEffectEntityCollision.attackId) != null;
    }

    /**
     * Is valid packet number of participants boolean.
     *
     * @param packetNumberOfParticipants the packet number of participants
     * @return the boolean
     */
    public static boolean isValidPacketNumberOfParticipants(final PacketNumberOfParticipants packetNumberOfParticipants) {
        return packetNumberOfParticipants.numberOfParticipants >= 1;
    }

    /**
     * Is valid packet player message boolean.
     *
     * @param packetPlayerMessage the packet player message
     * @return the boolean
     */
    public static boolean isValidPacketPlayerMessage(final PacketPlayerMessage packetPlayerMessage) {
        return packetPlayerMessage.message != null && packetPlayerMessage.message.length() <= MAX_MESSAGE_LENGTH;
    }

    /**
     * Is valid packet game over boolean.
     *
     * @param packetGameOver the packet game over
     * @return the boolean
     */
    public static boolean isValidPacketGameOver(final PacketGameOver packetGameOver) {
        return MultiplayerTeam.getMultiplayerTeamFromId(packetGameOver.loserTeamId) != null;
    }
}
