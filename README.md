# README #

### Project ###
* Slash the Cloud - Server

![ic_launcher.png](https://bitbucket.org/repo/aqaRge/images/3197734257-ic_launcher.png)

### Language ###
* [Java](https://fr.wikipedia.org/wiki/Java_(langage))

### Frameworks used ###
* [KryoNet](https://github.com/EsotericSoftware/kryonet)

### Plugins used ###
* [Project Lombok](https://projectlombok.org/)

### Build system ###
* [Maven](https://maven.apache.org/)

### See also ###
* [Slash the Cloud](https://bitbucket.org/plic2017/stc)

### Authors ###

* Nicolas Naudot
* David Baron

### Website ###
* [slashthecloud.bitbucket.org](http://slashthecloud.bitbucket.org/)

### Support ###

* slashthecloud@gmail.com
